define(['jquery'], function ($) {

    function init() {
        $('.modal_window').each(function () {
            var top = (($(window).height()) - ($(this).outerHeight())) / 2;
            var left = (($(window).width()) - ($(this).outerWidth())) / 2;

            $(this).css({'top': top, 'left': left});

        });

        $('.show_modal').click(function () {
            var name = $(this).attr('data-name');
            show(name);
        });

        $('.close_modal').click(function () {
            close();
        });
    }

    function show(name) {
        $('#mask').css({'display': 'block', opacity: 0});
        $('#mask').fadeTo(500, 0.8);
        $('#' + name).fadeIn(500);
    }


    function close() {
        $('#mask').fadeOut(500);
        $('.modal_window').fadeOut(500);
    }

    return {init: init}

});
