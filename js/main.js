requirejs.config({
    paths: {
        'jquery': 'libs/jquery.min',
        'QUnit': 'libs/qunit',
        'modalTest': 'tests/modalTest'
    },
    shim: {
        'QUnit': {
            exports: 'QUnit',
            init: function () {
                QUnit.config.autoload = false;
                QUnit.config.autostart = false;
            }
        }
    }
});

require(["modal", "QUnit", "modalTest"], function (modal, QUnit, modalTest) {
    modal.init();

    modalTest.run();
    QUnit.load();
    QUnit.start();
});
