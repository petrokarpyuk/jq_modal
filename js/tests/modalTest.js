define(['QUnit', 'jquery'], function (QUnit, $) {
    var run = function () {


        QUnit.test("Mask element exist", function (assert) {
            assert.ok($("#mask").length != 0, "Mask Element exist");
        });

        QUnit.test("Elements with such id exists", function (assert) {
            $(".show_modal").each(function (item) {
                var name = $(this).attr('data-name');
                assert.equal(name, $('#' + name).attr('id'), "El with such id exists")
            });
        });
        QUnit.test("Elements to show not empty", function (assert) {
            $(".show_modal").each(function (item) {
                var name = $(this).attr('data-name');
                assert.ok($('#' + name).text() || $('#' + name).children().length > 0, "Element is not empty");
            });
        });
    };


    return {run: run}

});